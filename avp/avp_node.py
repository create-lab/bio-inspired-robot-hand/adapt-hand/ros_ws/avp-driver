import rclpy
from rclpy.node import Node
from scipy.spatial.transform import Rotation as R
from helper_functions.utility import broadcast_tf
import rclpy.qos
from tf2_ros import TransformBroadcaster
from sensor_msgs.msg import Joy
from helper_functions.gamepad_functions import GamepadFunctions
from avp_stream import VisionProStreamer
from std_msgs.msg import Float64MultiArray, Float64
from .teleop import get_hand_joints
from helper_functions.hand_regulator import HandRegulator
import time

class AVPNode(Node):

    def __init__(self):
        super().__init__('avp_node')

        # tf related
        self.tf_broadcaster = TransformBroadcaster(self)
        
        # Hand related
        self.hand_regulator = HandRegulator()
        self.hand_servo_demand_publisher = self.create_publisher(Float64MultiArray, '/hand/servo_demand', rclpy.qos.qos_profile_sensor_data)
        self.binary_operation_publisher = self.create_publisher(Float64, '/grasp_percentage', 10)

        # avp related
        self.avp = VisionProStreamer(ip = "172.20.10.3", record = True)
        self.wrist_pose_publisher = self.create_publisher(Float64MultiArray, '/wrist/avp_demand', rclpy.qos.qos_profile_sensor_data)

        # Gamepad
        self.gp = GamepadFunctions()
        self.gamepad_subscriber = self.create_subscription(Joy, '/gamepad', self.gamepad_callback, rclpy.qos.qos_profile_sensor_data)

        # Main loop
        self.mainloop = self.create_timer(0.05, self.mainloop_callback) 

        self.latest = {}
    
    def pinch_dist_to_percentage(self, dist):
        max_dist = 0.1
        min_dist = 0.01

        dist = max(min_dist, min(max_dist, dist))

        percentage = (1/(min_dist - max_dist)) * (dist - max_dist)
        
        return percentage
    
    def broadcast_avp_frame(self, base_name, frame_name, data, broadcast = False):
        rot_matrix = data[:3,:3]
        trans = data[:3, 3]

        quat = R.as_quat(R.from_matrix(rot_matrix))

        if broadcast:
            broadcast_tf(base_name, frame_name, trans, quat, self.tf_broadcaster, self.get_clock().now().to_msg())

        self.latest[frame_name] = [trans, quat]

    def gamepad_callback(self, gamepad_raw_msg:Joy):
        self.gp.convert_joy_msg_to_dictionary(gamepad_raw_msg)

    def mainloop_callback(self):
        r = self.avp.latest
        # self.broadcast_avp_frame("world", "head", r["head"][0])
        # self.broadcast_avp_frame("world", "wrist_l", r["left_wrist"][0])

        self.broadcast_avp_frame("world", "wrist_r", r["right_wrist"][0], broadcast = True)

        fingers = ["Base", "Thumb_CMC", "Thumb_MCP", "Thumb_IP", "Thumb_TIP",
                   "Index_Base", "Index_MCP", "Index_PIP", "Index_DIP", "Index_TIP",
                   "Middle_Base", "Middle_MCP", "Middle_PIP", "Middle_DIP", "Middle_TIP",
                   "Ring_Base", "Ring_MCP", "Ring_PIP", "Ring_DIP", "Ring_TIP",
                   "Pinky_Base", "Pinky_MCP", "Pinky_PIP", "Pinky_DIP", "Pinky_TIP"]
        
        for i, name in enumerate(fingers):
            if name != "none":
                self.broadcast_avp_frame("wrist_r", name + "_r", r["right_fingers"][i], broadcast = False)
        
        est_hand_pos = get_hand_joints(self.latest)
        servo_pos = self.hand_regulator.hand_pos_to_servo_pos(est_hand_pos)

        if self.gp.button_data["L1"] == 1:
            
            tele_binary = False

            if not tele_binary:
            # avp --> full hand teleop
                msg = Float64MultiArray()
                msg.data = servo_pos
                self.hand_servo_demand_publisher.publish(msg)
                b_msg = Float64()
                b_msg.data = -1.0 # dummy value
                self.binary_operation_publisher.publish(b_msg)
            else:
                # avp --> binary operation
                msg = Float64()
                msg.data = self.pinch_dist_to_percentage(r["right_pinch_distance"])
                self.binary_operation_publisher.publish(msg)

            self.get_logger().info("publishing")
            
        msg = Float64MultiArray()
        msg.data = list(self.latest["wrist_r"][0]) + list(self.latest["wrist_r"][1])
        self.wrist_pose_publisher.publish(msg)

def main():
    rclpy.init()
    node = AVPNode()
    try:
        rclpy.spin(node)
    except KeyboardInterrupt:
        pass

    rclpy.shutdown()


